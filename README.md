# Cloner

Simple tool to manage git repositories. I use this to clone repositories into a
predictable folder structure and update all repositories. There are a few tools
like this. To clone a repository run

    cloner clone <url>
    
To update all repositories run.
    
    cloner update
    

# Install

    go install .

Make sure that you go install directory is part of the PATH variable. Normally
binaries are installed in `~/go/bin`.


# Roadmap

- Add init support? when cloning an empty repository I could init it in a
  language of my choice. Either through the init method of the language tooling
  or cookiecutter templates
