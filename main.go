package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"sync"

	"github.com/pelletier/go-toml"
	"github.com/urfave/cli/v2" // imports as package "cli"
)

type Config struct {
	Root string
}

func DefaultConfig() (Config, error) {
	homedir, err := os.UserHomeDir()
	if err != nil {
		return Config{}, err
	}
	configFolder := path.Join(homedir, ".config", "cloner")
	config := Config{}
	if _, err := os.Stat(configFolder); os.IsNotExist(err) {
		config = Config{fmt.Sprintf("%s/code", homedir)}
	} else {
		var bytes []byte
		configuration := path.Join(configFolder, "config.toml")
		if _, err := os.Stat(configuration); os.IsNotExist(err) {
			bytes = []byte(`
			Root="$HOME/code"
			`)
		} else {
			tomlFile, err := os.Open(configuration)
			if err != nil {
				return Config{}, err
			}
			defer tomlFile.Close()
			bytes, _ = ioutil.ReadAll(tomlFile)
		}
		toml.Unmarshal(bytes, &config)
	}
	return config, nil
}
func createFolder(tokens []string) string {
	var b strings.Builder
	for _, dir := range tokens[:len(tokens)-1] {
		fmt.Fprintf(&b, "%s/", dir)
	}
	fmt.Fprintf(&b, "%s", strings.TrimSuffix(tokens[len(tokens)-1], ".git"))
	return b.String()
}

func parseHTTP(repository string) string {
	return createFolder(strings.Split(repository, "/")[2:])
}

func parseSSH(repository string) string {
	s := strings.Split(repository, ":")
	domain := s[0][4:] // remove leading git@
	return fmt.Sprintf("%s/%s", domain, createFolder(strings.Split(s[1], "/")))
}

func Clone(config Config, repository string) error {
	var folder string
	if strings.HasPrefix(repository, "http") {
		folder = parseHTTP(repository)
	} else {
		folder = parseSSH(repository)
	}
	folder = fmt.Sprintf("%s/%s", config.Root, folder)
	err := exec.Command("git", "clone", repository, folder).Run()
	return err
}

func Update(config Config) error {
	fileList := make([]string, 0)
	err := filepath.Walk(config.Root, func(p string, f os.FileInfo, err error) error {
		gitfolder := path.Join(p, ".git")
		_, e := os.Stat(gitfolder)
		if e == nil {
			fileList = append(fileList, p)
		}
		return err
	})
	if err != nil {
		return err
	}
	var wg sync.WaitGroup
	for _, file := range fileList {
		wg.Add(1)
		go updateRepository(file, &wg)
	}
	wg.Wait()
	return nil
}

func updateRepository(path string, wg *sync.WaitGroup) error {
	defer wg.Done()
	fmt.Println(path)
	cmd := exec.Command("git", "remote", "update")
	cmd.Dir = path
	out, err := cmd.Output()
	if err != nil {
		return err
	}
	fmt.Printf("%s\n", out)
	return nil
}

func main() {
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:  "clone",
				Usage: "clone repository",
				Action: func(c *cli.Context) error {
					config, err := DefaultConfig()
					if err != nil {
						return err
					}
					return Clone(config, c.Args().Get(0))
				},
			},
			{
				Name:  "update",
				Usage: "update all repositories in root folder",
				Action: func(c *cli.Context) error {
					config, err := DefaultConfig()
					if err != nil {
						return err
					}
					return Update(config)
				},
			},
		}}
	app.Run(os.Args)
}
